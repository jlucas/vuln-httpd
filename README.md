# vuln-httpd

## Compilar
```
make
```

## Executar
```
./httpd
```

## Info
+ O servidor escuta no porto TCP 1234
+ A função _strcpy_, que escreve no buffer alocado para o User-Agent, é vulnerável a buffer overflows

### Exemplo de overflow
```
curl http://127.0.0.1:1234 -A AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
```
