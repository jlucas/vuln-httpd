#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <errno.h>

static volatile int running = 1;

int redeskfd, remobfd;
char redesk[10240], remob[10240];

void intHandler() {
  running = 0;
}

void respond( int connfd, FILE *connstream ) {

  // Create buffers
  char useragent[256];
  char line[1024];

  // Loop until we find the string "User-Agent: "
  while ( fgets( line, sizeof(line), connstream ) ) {
    if ( strstr( line, "User-Agent: " ) ) {
      // Copy user agent to buffer
      strcpy( useragent, line + sizeof("User-Agent:") );
      break;
    }
  }

  // Print debug info
  printf("Connection from user agent: %s", useragent);

  // Send response
  if ( strstr( useragent, "Android" ) )
    write( connfd, remob, strchr(remob, '\0') - remob );
  else
    write( connfd, redesk, strchr(redesk, '\0') - redesk );
}

void serve(int sock) {

  // Wait for connection and create file descriptor
  int connfd = accept(sock, (struct sockaddr*)NULL, NULL);

  // Convert file descriptor to FILE type
  FILE *connstream = fdopen(connfd, "r");

  // Check for valid request
  char line[1024];
  fgets( line, sizeof(line), connstream);
  if ( strstr( line, "GET /" ) != line ) {
    fclose(connstream);
    close(connfd);
    return;
  }

  respond( connfd, connstream );

  // Close connection
  fclose(connstream);
  close(connfd);
}

int main( int argc, char *argv[] ) {

  signal(SIGINT, intHandler);

  // Set working directory to binary directory
  chdir( dirname( argv[0] ) );

  // Create server socket
  struct sockaddr_in server;
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  int opt = 1;
  setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
  memset(&server, 0, sizeof(server));

  // Set up server for listening
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = htonl(INADDR_ANY);
  server.sin_port = htons(1234);
  bind(sock, (struct sockaddr*)&server, sizeof(server));
  printf("Bind: %s\n", strerror(errno));

  listen(sock, 10);

  // Open files containing response data
  redeskfd = open("response-desktop", O_RDONLY);
  remobfd = open("response-mobile", O_RDONLY);

  // Read responses into memory
  read( redeskfd, redesk, sizeof(redesk) );
  read( remobfd, remob, sizeof(remob) );

  while (running)
    serve(sock);

  close(sock);
  close(redeskfd);
  close(remobfd);

  printf("Gracefully stopping\n");

  return 0;
}
